from Constants import Constants

class Utils():

	def __init__(self):
		pass

	@staticmethod
	def get_response( arr ):
		max_val = 0
		index = 0
		for i in range(len(arr)):
			if arr[i] > max_val:
				max_val = arr[i]
				index = i
		return index

	@staticmethod
	def response_to_char( res ):
		return Constants.LETTER_CODES.get(res)
