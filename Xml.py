import os
from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
from xml.dom import minidom
from datetime import datetime
from random import randint


class Xml():
	def generate_xml(self, save_to, learning_rate, momentum_rate, iterations, hide_neurons, class_amount, data_size, inp, outp, wag ):
		print "learning_rate"
		print "momentum_rate"
		print "iterations"
		
		root = Element('ocr-data')
		tree = ElementTree(root)
		
		meta = Element('meta')
		created = Element('created')
		created.text = str(datetime.now())
		meta.append(created)


		config = Element('config')

		lr = learning_rate
		learning_rate = Element('learning_rate')
		learning_rate.text = str(lr)
		config.append(learning_rate)

		mr = momentum_rate
		momentum_rate = Element('momentum_rate')
		momentum_rate.text = str(mr)
		config.append(momentum_rate)

		it = iterations
		iterations = Element('iterations')
		iterations.text = str(it)
		config.append(iterations)

		ds = data_size
		data_size = Element('data_size')
		data_size.text = str(ds)
		config.append(data_size)

		ca = class_amount
		class_amount = Element('class_amount')
		class_amount.text = str(ca)
		config.append(class_amount)

		hn = hide_neurons
		hidden_neurons = Element('hidden_neurons')
		hidden_neurons.text = str(hn)
		config.append(hidden_neurons)


		layers = Element('layers')

		for i in range(0,2):
			# l = layers[i]
			layer = Element('layer')
			layer.set('id',str(i))
			# size = Element('size')
			# size.text = str(len(l))
			# layer.append(size)
			data = Element('data')
			wages = Element('wages')
			wages.set('amount',str(len(wag[i])))
			for w in wag[i]:
				wage = Element('wage')
				wage.text = str(w)
				wages.append(wage)
			inpx = Element('inputs')
			inpx.set('amount',str(len(inp[i])))
			for inpu in inp[i]:
				inpxx = Element('input')
				inpxx.text = str(inpu)
				inpx.append(inpxx)
			outpx = Element('outputs')
			outpx.set('amount',str(len(outp[i])))
			for out in outp[i]:
				outpxx = Element('output')
				outpxx.text = str(out)
				outpx.append(outpxx)
			data.append(wages)
			data.append(inpx)
			data.append(outpx)
			layer.append(data)
			layers.append(layer)

		root.append(meta)
		root.append(config)
		root.append(layers)

		preety_xml = minidom.parseString(etree.tostring(root)).toprettyxml(indent="   ")
		with save_to as f:
			f.write(preety_xml)


# xml = Xml().generate_xml()