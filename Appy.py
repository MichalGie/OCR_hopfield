import sys
import os
import glob

from BackpropNeuralNetwork import BackpropNeuralNetwork
from Constants import Constants
from Xml import Xml
import datetime

from TransformCharacter import TransformCharacter
from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
from Utils import Utils

class App():

	def __init__(self):
		self.learning_progress = 0

	def learn(self, learn_data_path=None,learning_rate=None, momentum_rate=None, iterations=None, data_size=None, hide_neurons=None, class_amount=None):
		self.learn_data_path = learn_data_path
		# self.test_data_path = test_data_path
		self.learning_rate = learning_rate
		self.momentum_rate = momentum_rate
		self.iterations = iterations
		self.data_size = data_size
		self.hide_neurons = hide_neurons
		self.class_amount = class_amount
		self.initialize(self)

	@staticmethod
	def initialize(self):

		CLASES = self.class_amount

		print "Collecting data..."

		tch = TransformCharacter()

		files = glob.glob(os.path.join(self.learn_data_path,"*.bmp"))

		data_sorted  = []
		for elem in (sorted(files)):
			data_sorted.append(elem)

		training_size  = len(data_sorted)

		data_to_train = []*training_size

		for i in range(training_size):
			print(data_sorted[i])
			data_to_train.append(tch.bmpToBits(data_sorted[i]))

		self.training_data  = (
			data_to_train
			)


# TO DOBRZE 
		self.training_result = []

		# print training_size
		s = 0
		for i in range(training_size/CLASES):
			s = 0
			for j in range(CLASES):
				st_temp = [0]*CLASES
				st_temp[s] = 1
				self.training_result.append(st_temp)
				s += 1

	def get_progress(self):
		return self.learning_progress
	

	def load_wages(self,path_to_wages):
		print "wages..."

		et = ElementTree()
		root = et.parse(path_to_wages)

		neurons = []
		inputs = [list(),list()]
		outputs = [list(),list()]
		wages = [list(),list()]

		hide_neurons = int(root.find('config/hidden_neurons').text)
		print(hide_neurons)

		size = int(root.find('config/data_size').text)
		print(size)

		class_amount = int(root.find('config/class_amount').text)
		print(class_amount)


		i = 0;
		for layer in root.findall('layers/layer'):
			# size = layer.find('size').text
			# neurons.append(size)
			for wage in layer.findall('data/wages/wage'):
				wages[i].append(float(wage.text))
			for input in layer.findall('data/inputs/input'):
				inputs[i].append(float(input.text))
			for output in layer.findall('data/outputs/output'):
				outputs[i].append(float(output.text))
			i = i+1


		bnn = BackpropNeuralNetwork(size,hide_neurons,class_amount,inputs,outputs,wages)

		return bnn
		# print(neurons)
		# print(wages)



	def start_learning(self):
		print "Learning...."
		start = datetime.datetime.now()
		bnn = BackpropNeuralNetwork(self.data_size,self.hide_neurons,self.class_amount)

		for iteration in range(self.iterations):
			for i in range(len(self.training_result)):
				bnn.train(
					self.training_data[i],
					self.training_result[i],
					self.momentum_rate,
					self.momentum_rate,
					)

			# if self.progress:
			# print(self.iterations)
			if ((iteration/float(self.iterations))*100) % 4:
				self.learning_progress = (iteration/float(self.iterations))*100
				print "----------progress %.3f%%"%(self.learning_progress)
				# print len(bnn.getWages(0))
				# print len(bnn.getWages(1))
				# for el in bnn.getWages():
					# print el,
					# for t in range(len(training_result)):
					# 	a  = training_result[t]
					# 	td = training_data[t]
					# 	out = bnn.run(td)
					# 	print a,out
			# 			# print "%.1f, %.1f, %.1f %.1f, %.1f  ---> %.3f , %.3f, %.3f , %.3f, %.3f"%(a[0],a[1],a[2],a[3],a[4],out[0],out[1],out[2],out[3],out[4])
			# elif iteration >= Constants.ITERATIONS-1:
			# 	end = datetime.datetime.now()
			# 	delta = end - start
			# 	print "----------time %d ms"%(int(delta.total_seconds() * 1000))
			# 	print "----------Iteration %d"%(iteration)
				# a = None
				# td = None
				# out = None
				letter_code = 0
				for t in range(len(self.training_result)):
					if letter_code > 22:
						letter_code = 0
					a  = self.training_result[t]
					td = self.training_data[t]
					out = bnn.run(td)
					#print "-----------------%s-----------------"%(Constants.LETTER_CODES.get(letter_code))
					#for i in range(23):
					#	print "%.3f --> %.3f \n"%(a[i],out[i])
					letter_code += 1
					# print "%.1f, %.1f, %.1f %.1f, %.1f  ---> %.3f , %.3f, %.3f , %.3f, %.3f"%(a[0],a[1],a[2],a[3],a[4],out[0],out[1],out[2],out[3],out[4])
		end = datetime.datetime.now()
		delta = end - start
		print "----------time %d ms"%(int(delta.total_seconds() * 1000))
		self.learning_progress = 100
		# print "----------iterations: %d"%(self.iterations)

	
		return bnn

	def generate_xml(self, bnn,save_to):
		inp1 = bnn.getInput(0)
		inp2 = bnn.getInput(1)
		inp = (inp1,inp2)

		out1 = bnn.getOutput(0)
		out2 = bnn.getOutput(1)
		out = (out1,out2)

		w1 = bnn.getWages(0)
		w2 = bnn.getWages(1)
		wages = (w1,w2)

		xml = Xml()
		xml.generate_xml(save_to, self.learning_rate, self.momentum_rate, self.iterations, self.hide_neurons, self.class_amount, self.data_size, inp, out, wages)

	def start_recognize(self, bnn,test_data_path,save_to,save_xml=None):

		if save_xml:
			self.generate_xml(bnn,save_xml)

		files = glob.glob(os.path.join(test_data_path,"*.bmp"))

		print(len(files))
		files_data_sorted = []


		for elem in (sorted(files)):
			files_data_sorted.append(elem)

		data_size  = len(files_data_sorted)


		data_to_train = []*data_size

		print(data_size)
		tch = TransformCharacter()
		for i in range(data_size):
			data_to_train.append(tch.bmpToBits(files_data_sorted[i]))

		data = (
			data_to_train
		)

		response = ''

		klasa = 0
		zle = 0
		for d in range(len(data)):
			if klasa > 22:
				klasa = 0
			
 			dt = data[d]
			out = bnn.run(dt)

			res = Utils.get_response( out )
			char = Utils.response_to_char( res )

			response += char
		
			klasa += 1

		with save_to as f:
			f.write(response)

		print('finished recogniton')

		return bnn

	def get_layer(sel):
		return 

# if __name__ == "__main__":
# 	App(sys.argv[1:])
