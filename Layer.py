import random
from ActivationFunction import ActivationFunction

class Layer():

	def __init__(self, inpit_size, output_size, input_to_load=None, output_to_load=None, wages_to_load=None):
		self.__output = [0] * output_size
		self.__input = [0] * (inpit_size+1)
		self.__weights = [0]*((inpit_size+1)*output_size)
		self.__d_weights = [0]*len(self.__weights)

		if not wages_to_load:
			self.__initWeights()
		else:
			self.__laodWeights(input_to_load,output_to_load,wages_to_load)

	def __initWeights(self):
		for i in range(len(self.__weights)):
			self.__weights[i] = random.uniform(-2,2)

	def __laodWeights(self,input,output,wages):
		print len(self.__output)
		print len(wages)
		for i in range(len(self.__weights)):
			self.__weights[i] = wages[i]
		for i in range(len(self.__output)):
			self.__output[i] = output[i]
		for i in range(len(self.__input)):
			self.__input[i] = input[i]


	def run(self, inputList ):
		for i in range(len(self.__input)-1):
			self.__input[i] = inputList[i]

		# print type(input[1])
		# print "check"
		# print inputList
		# print(len(input))
		self.__input[len(self.__input)-1] = 1

		offset = 0

		# for i in range(len(self.__weights)):
			# print type(self.__weights[i])
			# print "waga"
			# print self.__weights[i]



		for i in range( len(self.__output )):
			for j in range(len(self.__input)):
				# print self.__weights[2]
				# print '---**'
				# print self.__input
				# print self.__input[2]
				# print '---'
				# print self.__input[0]
				# print len(input)
				# print i
				# print "wagi:",
				# print self.__weights
				# print "rozmiar wagg:",
				# print len(self.__weights)
				# print "output",
				# print self.__output
				# print "rozmiar_out",
				# print len(self.__output)
				# print self.__weights[0+2]
				# print len(input)
				# print "j %d"%(j)
				# print "offset %d"%(offset)
				self.__output[i] = self.__output[i] + self.__weights[offset+j]*self.__input[j]
			self.__output[i] = ActivationFunction.sigmoid(self.__output[i])
			offset += len(self.__input)


		out = []
		out.extend(self.__output)

		# print "run out layer"
		# print self.__output

		return out


	def train(self, error, learning_rate, momentum ):

		offset = 0
		weight_index = 0
		next_error = [0]*len(self.__input)
		# print "INPUT:", self.__input

		for i in range(len(self.__output)):
			delta = error[i] * ActivationFunction.dSigmoid(self.__output[i])
			# print "error",error[i]
			# print "--------delat-----",delta

			for j in range(len(self.__input)):
				weight_index = offset + j
				# print "------------we index:  ",weight_index
				# print "------------nex errsize  ",len(next_error)
				# print "s--------elf.__weights", len(self.__input)


				next_error[j] = next_error[j] + self.__weights[weight_index]*delta

				dw = self.__input[j] * delta * learning_rate
				# print "input",self.__input[j] 
				# if self.__input[j] > 0:
				# 	return
				# print "-----------------------dw---------------",dw
				# print "-------------------mm-------------------",momentum
				# print "--------------------wg-------------------",self.__weights[weight_index]
				self.__weights[weight_index] += self.__d_weights[weight_index] * momentum + dw
				# print "wg-------",self.__weights[weight_index]
				self.__d_weights[weight_index] = dw


			offset += len(self.__input)
			# print "layer next error"
			# print (next_error)
		return next_error

	def getWeights(self,i):
		return self.__weights

	def getInput(self,i):
		return self.__input

	def getOutput(self,i):
		return self.__output