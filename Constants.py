
class Constants():

	def __init__(self):
		pass

	LEARNING_RATE = 0.3
	MOMENTUM = 0.6
	ITERATIONS = 1050
	HIDDEN_NEURONS = 45

	LETTER_CODES = {
			0:'A',
			1:'B',
			2:'C',
			3:'D',
			4:'E',
			5:'F',
			6:'G',
			7:'H',
			8:'I',
			9:'J',
			10:'K',
			11:'L',
			12:'M',
			13:'N',
			14:'O',
			15:'P',
			16:'R',
			17:'S',
			18:'T',
			19:'U',
			20:'W',
			21:'Y',
			22:'Z',
			# 23:'O/',
			# 24:'A/',
			# 25:'C/',
			# 26:'E/',
			# 27:'L/',
			# 28:'N/',
			# 29:'S/',
			# 30:'Z/',
			# 31:'Z.',
	}
