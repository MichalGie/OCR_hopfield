import math

class ActivationFunction():

	@staticmethod
	def sigmoid( x ):
		return (float) (1 / (1+math.exp(-x)))

	@staticmethod
	def dSigmoid( x ):
		return x*(1-x)