import os, sys
import Tkinter as tk
from tkFileDialog import askopenfilename, asksaveasfile
import time
from multiprocessing.pool import ThreadPool
from ImageTranform import Obrazek
from Appy import App
from Constants import Constants
import time
import tkMessageBox

class Gui():
    def __init__(self):

        self.inactive = "#2A3129"
        self.active = "#088DA5"
        self.complete = "#329932"
        self.active_elements = "white"
        self.inactive_elements = "#2A3129"

        self.file_to_save_result = None
        self.file_to_save_xml = None
        self.learn_data_count = None
        self.learn_data_path = None


        self.test_file = None
        self.test_data_path = None

        self.wages_file = None
        self.learned = False

        self.app = App()
        self.bnn = None

        self.root = tk.Tk()
        # self.root.wm_attributes('-transparentcolor','yellow')
        self.root.title("some application")
        self.root.minsize(width=750,height=550)
        self.root.maxsize(width=750,height=550)

        cons = Constants()

        # menu left
        self.menu_left = tk.Frame(self.root, width=150, bg="#ababab")
        self.menu_left_upper = tk.Frame(self.menu_left, width=250,  height=200, bg=self.inactive, bd=0, highlightbackground="white", highlightcolor="white", highlightthickness=2)
        self.menu_left_lower = tk.Frame(self.menu_left, width=250,  height=200, bg=self.inactive, bd=0, highlightbackground="white", highlightcolor="white", highlightthickness=2)

        self.menu_left_upper.pack_propagate(0)
        self.menu_left_lower.pack_propagate(0)
        self.menu_left_upper.pack(side="top", fill="both", expand=True)
        self.menu_left_lower.pack(side="top", fill="both", expand=True)

        self.menu_left_upper_label = tk.Label( self.menu_left_upper, text="Wybierz plik z danymi do nauki")
        # self.menu_left_upper_label.attributes("-alpha", .30)
        self.menu_left_upper_label.pack(fill="both")

        self.open_learn_file = tk.Button(self.menu_left_upper, text="Wybierz plik", command=self.load_learn_file)
        self.open_learn_file.pack(pady=(50,10))

        self.learn_file_label = tk.Label(self.menu_left_upper, text="Wybrano plik: ")
        self.learn_file_label.pack(anchor="w", pady=(10,10))

        self.learn_file_count_label = tk.Label(self.menu_left_upper, text="Ilosc danych: ")
        self.learn_file_count_label.pack(anchor="w", pady=(10,10))

        self.menu_left_lower_label = tk.Label(  self.menu_left_lower, text="Wybierz plik z wagami")
        self.menu_left_lower_label.pack(fill="both")

        self.open_wages_file = tk.Button(self.menu_left_lower, text="Wybierz plik", command=self.load_wages_file)
        self.open_wages_file.pack(pady=(50,10))

        self.wages_file_label = tk.Label(self.menu_left_lower, text="Wybrano plik: ")
        self.wages_file_label.pack(side="left", pady=(10,10))

        self.menu_left.grid(row=0, column=0, rowspan=2, sticky="nsew")
       
        # menu center
        self.menu_center = tk.Frame(self.root, width=150, bg="#ababab")
        self.menu_center_upper = tk.Frame(self.menu_center, width=250, height=200, bd=0, highlightbackground="white", highlightcolor="white", highlightthickness=2)
        self.menu_center_lower = tk.Frame(self.menu_center, width=250, height=200, bd=0, highlightbackground="white", highlightcolor="white", highlightthickness=2)

        # upper
        self.menu_center_upper_label = tk.Label(  self.menu_center_upper, text="Wybierz ustawienia uczenia" )
        self.menu_center_upper_label.pack(fill="both",pady=(0,20))


        self.learning_class_val = 23
        self.learning_class_frame = tk.Frame( self.menu_center_upper )
        self.learning_class_frame.pack(anchor='w', pady=5)
        self.learning_class_label = tk.Label(self.learning_class_frame, text="Liczba klas:")
        self.learning_class_label.pack(side='left')
        self.learning_class = tk.Entry(self.learning_class_frame, width=5, textvariable=self.learning_class_val)
        self.learning_class.insert(tk.END,self.learning_class_val)
        self.learning_class.pack(side='left')


        self.learning_rate_val = cons.LEARNING_RATE
        self.learning_rate_frame = tk.Frame( self.menu_center_upper )
        self.learning_rate_frame.pack(anchor='w', pady=5)
        self.learning_rate_label = tk.Label(self.learning_rate_frame, text="Learning rate:")
        self.learning_rate_label.pack(side='left')
        self.learning_rate = tk.Entry(self.learning_rate_frame, width=5, textvariable=self.learning_rate_val)
        self.learning_rate.insert(tk.END,self.learning_rate_val)
        self.learning_rate.pack(side='left')

        self.momentum_rate_val = cons.MOMENTUM
        self.momentum_rate_frame = tk.Frame( self.menu_center_upper )
        self.momentum_rate_frame.pack(anchor='w',pady=5)
        self.momentum_rate_label = tk.Label(self.momentum_rate_frame, text="Momentum rate:")
        self.momentum_rate_label.pack(side='left')
        self.momentum_rate = tk.Entry(self.momentum_rate_frame, width=5, textvariable=self.momentum_rate_val)
        self.momentum_rate.insert(tk.END,self.momentum_rate_val)
        self.momentum_rate.pack(side='left')

        self.hidden_neurons_val = cons.HIDDEN_NEURONS
        self.hidden_neurons_frame = tk.Frame( self.menu_center_upper )
        self.hidden_neurons_frame.pack(anchor='w',pady=5)
        self.hidden_neurons_label = tk.Label(self.hidden_neurons_frame, text="Hidden neurons:")
        self.hidden_neurons_label.pack(side='left')
        self.hidden_neurons = tk.Entry(self.hidden_neurons_frame, width=5, textvariable=self.hidden_neurons_val)
        self.hidden_neurons.insert(tk.END,self.hidden_neurons_val)
        self.hidden_neurons.pack(side='left')

        self.iterations_val = cons.ITERATIONS
        self.iterations_frame = tk.Frame( self.menu_center_upper )
        self.iterations_frame.pack(anchor='w',pady=5)
        self.iterations_label = tk.Label(self.iterations_frame, text="Iterations:")
        self.iterations_label.pack(side='left')
        self.iterations = tk.Entry(self.iterations_frame, width=5, textvariable=self.iterations_val)
        self.iterations.insert(tk.END,self.iterations_val)
        self.iterations.pack(side='left')

        self.accept_config_button = tk.Button(self.menu_center_upper, text="Zatwierdz konfiguracje",  command=self.accept_learn_cofnig)
        self.accept_config_button.pack(pady=5)

        # lower
        self.menu_center_lower_label = tk.Label( self.menu_center_lower, text="Wybierz plik z danymi do rozpoznania")
        self.menu_center_lower_label.pack(fill="both")

        self.open_test_file = tk.Button(self.menu_center_lower, text="Wybierz plik", command=self.load_test_file)
        self.open_test_file.pack(pady=(50,10))

        self.test_file_label = tk.Label(self.menu_center_lower, text="Wybrano plik: ")
        self.test_file_label.pack(anchor='w', pady=(10,10))

        self.test_file_amount_label = tk.Label(self.menu_center_lower, text="Wyodrebniono: ")
        self.test_file_amount_label.pack(anchor='w', pady=(10,10))

        self.menu_center_upper.pack_propagate(0)
        self.menu_center_lower.pack_propagate(0)
        self.menu_center_upper.pack(side="top", fill="both", expand=True)
        self.menu_center_lower.pack(side="top", fill="both", expand=True)

        self.menu_center.grid(row=0, column=1, rowspan=2, sticky="nsew")
        

        # menu right
        self.menu_right = tk.Frame(self.root, width=150, bg="#ababab")
        self.menu_right_upper = tk.Frame(self.menu_right, width=250, height=200, bd=0, highlightbackground="white", highlightcolor="white", highlightthickness=2)
        self.menu_right_lower = tk.Frame(self.menu_right, width=250, height=200, bd=0, highlightbackground="white", highlightcolor="white", highlightthickness=2)

        # UPPER
        self.menu_right_upper_label = tk.Label( self.menu_right_upper, text="Uczenie")
        self.menu_right_upper_label.pack(fill="both")

        self.start_learn_file = tk.Button(self.menu_right_upper, text="Zacznij uczyc!", command=self.start_learn)
        self.start_learn_file.pack(pady=(50,10))

        self.learn_progress = tk.Label(self.menu_right_upper, text="0%")
        self.learn_progress.pack(pady=(10,10))

        # lower
        self.menu_right_lower_label = tk.Label( self.menu_right_lower, text="Rozpoznawaj")
        self.menu_right_lower_label.pack(fill="both")

        self.save_xml = tk.Button(self.menu_right_lower, text="Zapisz konfiguracje sieci", command=self.save_xml)
        self.save_xml.pack(pady=(40,10))

        self.save_file = tk.Button(self.menu_right_lower, text="Wybierz plik do zapisu danych", command=self.file_to_save)
        self.save_file.pack(pady=(40,10))

        self.start_test = tk.Button(self.menu_right_lower, text="Rozpoznaj!", command=self.start_test)
        self.start_test.pack(pady=(40,10))

        self.menu_right_upper.pack_propagate(0)
        self.menu_right_lower.pack_propagate(0)
        self.menu_right_upper.pack(side="top", fill="both", expand=True)
        self.menu_right_lower.pack(side="top", fill="both", expand=True)

        self.menu_right.grid(row=0, column=2, rowspan=2, sticky="nsew")


        # self.status_frame.grid(row=2, column=0, columnspan=6, sticky="ew")

        self.root.grid_rowconfigure(1, weight=1)
        self.root.grid_columnconfigure(1, weight=1)

        self.activate_left_lower_menu( True, True )
        self.activate_left_upper_menu( True, True )
        self.activate_center_upper_menu( False, False )
        self.activate_center_lower_menu( False, False )
        self.activate_right_upper_menu( False, False )
        self.activate_right_lower_menu( False, False )

        self.root.mainloop()

    def activate_left_upper_menu( self, activate , complete ):
        if activate is True:
            self.menu_left_upper.config(bg=self.active)
            self.menu_left_upper_label.config(bg=self.active_elements)
            self.open_learn_file.config(bg=self.active_elements)
            self.learn_file_label.config(bg=self.active_elements)
            self.learn_file_count_label.config(bg=self.active_elements)
        else:
            if complete is True:
                self.menu_left_upper.config(bg=self.complete)
                self.activate_left_lower_menu( False, False )
            else:
                self.menu_left_upper.config(bg=self.inactive_elements)
                self.menu_left_upper_label.config(bg=self.inactive_elements)
                self.open_learn_file.config(bg=self.inactive_elements)
                self.learn_file_label.config(bg=self.inactive_elements)
                self.learn_file_count_label.config(bg=self.inactive_elements)
            self.open_learn_file.config(state="disabled")
            
    def activate_left_lower_menu( self, activate, complete ):
        if activate is True:
            self.menu_left_lower.config(bg=self.active)
            self.menu_left_lower_label.config(bg=self.active_elements)
            self.open_wages_file.config(bg=self.active_elements)
            self.wages_file_label.config(bg=self.active_elements)
        else:
            if complete is True:
                 self.menu_left_lower.config(bg=self.complete)
                 self.activate_left_upper_menu( False, False )
            else:
                self.menu_left_lower.config(bg=self.inactive)
                self.menu_left_lower_label.config(bg=self.inactive_elements)
                self.open_wages_file.config(bg=self.inactive_elements)
                self.wages_file_label.config(bg=self.inactive_elements)

            self.open_wages_file.config(state="disabled")

            

    def activate_center_upper_menu( self, activate, complete ):
        if activate is True:
            self.menu_center_upper.config(bg=self.active)
            self.menu_center_upper_label.config(bg=self.active_elements)
            self.accept_config_button.config(bg=self.active_elements)
            self.learning_class_label.config(bg=self.active_elements)
            self.learning_class.config(bg=self.active_elements)
            self.hidden_neurons_label.config(bg=self.active_elements)
            self.hidden_neurons.config(bg=self.active_elements)
            self.learning_rate_label.config(bg=self.active_elements)
            self.momentum_rate_label.config(bg=self.active_elements)
            self.iterations_label.config(bg=self.active_elements)
            self.momentum_rate.config(bg=self.active_elements)
            self.learning_rate.config(bg=self.active_elements)
            self.iterations.config(bg=self.active_elements)
            self.learning_class.config(state="normal")
            self.hidden_neurons.config(state="normal")
            self.momentum_rate.config(state="normal")
            self.learning_rate.config(state="normal")
            self.iterations.config(state="normal")
            self.accept_config_button.config(state="normal")
        else:
            if complete is True:
                self.menu_center_upper.config(bg=self.complete)
            else:
                self.menu_center_upper.config(bg=self.inactive)
                self.menu_center_upper_label.config(bg=self.inactive_elements)
                self.learning_class_label.config(bg=self.inactive_elements)
                self.learning_class.config(bg=self.inactive_elements)
                self.hidden_neurons_label.config(bg=self.inactive_elements)
                self.hidden_neurons.config(bg=self.inactive_elements)
                self.learning_rate_label.config(bg=self.inactive_elements)
                self.momentum_rate_label.config(bg=self.inactive_elements)
                self.iterations_label.config(bg=self.inactive_elements)
                self.accept_config_button.config(bg=self.inactive_elements)
                self.learning_class.config(bg=self.inactive_elements)
                self.momentum_rate.config(bg=self.inactive_elements)
                self.learning_rate.config(bg=self.inactive_elements)
                self.iterations.config(bg=self.inactive_elements)
            self.learning_class.config(state="disabled")
            self.hidden_neurons.config(state="disabled")
            self.momentum_rate.config(state="disabled")
            self.learning_rate.config(state="disabled")
            self.iterations.config(state="disabled")
            self.accept_config_button.config(state="disabled")

    def activate_center_lower_menu( self, activate, complete ):
        if activate is True:
            self.menu_center_lower.config(bg=self.active)
            self.menu_center_lower_label.config(bg=self.active_elements)
            self.open_test_file.config(bg=self.active_elements)
            self.test_file_label.config(bg=self.active_elements)
            self.test_file_amount_label.config(bg=self.active_elements)
            self.open_test_file.config(state="normal")
        else:
            if complete is True:
                self.menu_center_lower.config(bg=self.complete)
            else:
                self.menu_center_lower.config(bg=self.inactive)
                self.menu_center_lower_label.config(bg=self.inactive_elements)
                self.open_test_file.config(bg=self.inactive_elements)
                self.test_file_label.config(bg=self.inactive_elements)
                self.test_file_amount_label.config(bg=self.inactive_elements)
            self.open_test_file.config(state="disabled")

    def activate_right_upper_menu( self, activate, complete ):
        if activate is True:
            self.menu_right_upper.config(bg=self.active)
            self.menu_right_upper_label.config(bg=self.active_elements)
            self.start_learn_file.config(bg=self.active_elements)
            self.learn_progress.config(bg=self.active_elements)
            self.start_learn_file.config(state='normal')
        else:
            if complete is True:
                self.menu_right_upper.config(bg=self.complete)
            else:
                self.menu_right_upper.config(bg=self.inactive)
                self.menu_right_upper_label.config(bg=self.inactive_elements)
                self.start_learn_file.config(bg=self.inactive_elements)
                self.learn_progress.config(bg=self.inactive_elements)
            self.start_learn_file.config(state='disabled')

    def activate_right_lower_menu( self, activate, complete ):

        if ( self.test_file and self.learned ) or ( self.wages_file and self.test_file ):
            if activate is True:
                self.menu_right_lower.config(bg=self.active)
                self.save_file.config(state="normal")
                if self.wages_file is None:
                    self.save_xml.config(state="normal")
                    self.save_xml.config(bg=self.active_elements)
                self.start_test.config(state="disable")
                self.save_file.config(bg=self.active_elements)
                self.menu_right_lower_label.config(bg=self.active_elements)
            elif complete is True:
                    self.menu_right_lower.config(bg=self.complete)
        else:
            self.menu_right_lower.config(bg=self.inactive)
            self.save_file.config(bg=self.inactive_elements)
            self.save_xml.config(bg=self.inactive_elements)
            self.start_test.config(bg=self.inactive_elements)
            self.menu_right_lower_label.config(bg=self.inactive_elements)
            self.save_file.config(state="disable")
            self.save_xml.config(state="disable")
            self.start_test.config(state="disable")

    def accept_learn_cofnig(self):
        if self.learning_class.get() and self.learning_rate.get() and self.momentum_rate.get() and self.iterations.get():
            if self.learn_data_count % int(self.learning_class.get()) == 0: 
                self.activate_center_upper_menu( False, True )
                self.activate_right_upper_menu( True, False)
                self.activate_center_lower_menu( True, False )

    def load_learn_file(self):
        file = askopenfilename(filetypes=[("Images files", "*.bmp")])

        if file:
            text = self.learn_file_label['text'] + os.path.basename(file)
            self.learn_file_label.config(text=text)
            self.open_learn_file.config(state='disable')

            o = Obrazek(file)
            self.learn_data_path = o.prepare_folder()
            data_count = o.tnij()

            self.learn_data_count =  data_count

            text = self.learn_file_count_label['text'] + str(data_count)
            self.learn_file_count_label.config(text=text)


            self.activate_left_upper_menu( False, True )
            self.activate_center_upper_menu( True, False )
            self.learn_file = file

    def start_test(self):
        self.start_test.config(state="disable")

        print 'testing'
        # pool = ThreadPool(processes=1)
        self.app.start_recognize(self.bnn,self.test_data_path,self.file_to_save_result, self.file_to_save_xml)
        # test = async_result.get()

        if tkMessageBox.askokcancel("Rozpoznano pomyslnie", "Rozpoznawanie zakonczone. Otworzyc plik wynikow?"):
          os.system("gedit " + self.file_to_save_result.name)
        os.execl(sys.executable, sys.executable, *sys.argv)

        

    def save_xml(self):
        file = asksaveasfile(filetypes=[("Wages files", "*.xml")])

        if file:
            print file
            self.file_to_save_xml = file
            self.save_xml.config(state="disable")
            # self.start_test.config(state="normal")
            # self.start_test.config(bg=self.active_elements)

    def file_to_save(self):
        file = asksaveasfile(filetypes=[("Text files", "*.txt")])

        if file:
            print file
            # text = self.learn_file_label['text'] + os.path.basename(file)
            # self.learn_file_label.config(text=text)

            self.file_to_save_result = file
            self.save_file.config(state="disable")
            self.start_test.config(state="normal")
            self.start_test.config(bg=self.active_elements)
            # self.activate_left_upper_menu( False, True )
            # self.activate_center_upper_menu( True, False )

    def start_learn(self):
        self.start_learn_file.config(state="disabled")

        learning_rate = float(self.learning_rate.get())
        momentum_rate = float(self.momentum_rate.get())
        iterations = int(self.iterations.get())
        hidden_neurons = int(self.hidden_neurons.get())
        size = 80
        # hidden_neurons = 45
        classes = int(self.learning_class.get())

        print iterations

        self.app.learn(self.learn_data_path,learning_rate, momentum_rate,iterations,size,hidden_neurons,classes)
        pool = ThreadPool(processes=1)
        async_result = pool.apply_async(self.app.start_learning)

        progress = self.app.get_progress()
        print(progress)
        while( progress < 100 ):
            progress = self.app.get_progress()
            print(progress)
            self.learn_progress.config(text='%.1f%%'%(progress))
            self.root.update()
            time.sleep( 5 )

        # get_progress
        # self.bnn = self.app.start_learning()


        # i = 0
        # while( i<100 ):
        #     self.learn_progress.config(text=i)
        #     self.root.update()
        #     i = i +1
        #     print i
            # time.sleep(1)

        self.bnn = async_result.get()

        self.learned = True
        self.activate_right_upper_menu(False,True)
        self.activate_right_lower_menu(True,False)


    def load_test_file(self):
        file = askopenfilename(filetypes=[("Images files", "*.bmp")])

        if file:
            text = self.test_file_label['text'] + os.path.basename(file)
            self.test_file_label.config(text=text)

            o = Obrazek(file)
            self.test_data_path = o.prepare_folder()
            data_count = o.tnij()

            self.test_data_count =  data_count

            text = self. test_file_amount_label['text'] + str(data_count)
            self.test_file_amount_label.config(text=text)

            self.test_file = file


            self.activate_center_lower_menu( False, True )
            self.activate_right_lower_menu( True, False )

    def load_wages_file(self):
        file = askopenfilename(filetypes=[("Wages files", "*.xml")])

        if file:
            text = self.learn_file_label['text'] + os.path.basename(file)
            self.wages_file_label.config(text=text)
            
            self.wages_file = file
            self.activate_left_lower_menu( False, True )
            self.activate_center_upper_menu( False, False )
            self.activate_center_lower_menu( True, False )

            # pool = ThreadPool(processes=1)
            # async_result = pool.apply_async(app.load_wages, (file))
            self.bnn = self.app.load_wages(file)


Gui()