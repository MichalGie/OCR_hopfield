from Layer import Layer


class BackpropNeuralNetwork():

	def __init__(self, input_size, hidden_size, output_size, input_to_load=None, output_to_load=None, wages_to_load=None):
		self.__layers = [Layer]*2

		if not wages_to_load:
			self.__layers[0] = Layer( input_size, hidden_size)
			self.__layers[1] = Layer( hidden_size, output_size)
		else:
			self.__layers[0] = Layer( input_size, hidden_size, input_to_load[0], output_to_load[0], wages_to_load[0] )
			self.__layers[1] = Layer( hidden_size, output_size,input_to_load[1], output_to_load[1], wages_to_load[1] )


	def getLayer(self ):
		return self.__layers

	def getWages(self, i):
		return self.__layers[i].getWeights(i)

	def getInput(self, i):
		return self.__layers[i].getInput(i)

	def getOutput(self, i):
		return self.__layers[i].getOutput(i)


	def run(self, input ):
		activations = input

		# print "aa"
		# print activations

		for i in range(len(self.__layers)):
			activations = self.__layers[i].run(activations)

		# print "activations"
		return activations 


	def train(self, input, targetOutput, learning_rate, momentum ):
		calculateOutput = self.run(input)
		error = [float]*len(calculateOutput)

		for i in range(len(error)):
			error[i] = targetOutput[i] - calculateOutput[i]

		for i in range((len(self.__layers)-1),0,-1):
			error = self.__layers[i].train(error,learning_rate,momentum)